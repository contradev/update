#!/bin/sh

clear

echo "========================"
echo "Contra WordPress Updates"
echo "========================"

git checkout updates
git fetch --all
git reset --hard origin/master
git pull --no-edit origin updates
git push origin updates

DATE=$(date +"%m/%d/%Y %r")
COREUPDATE=$(wp core check-update)
PLUGINUPDATE=$(wp plugin update --all --dry-run --format=summary)
SITE=$(wp option get siteurl)

if $(wp core is-installed); then
    if [[ $(wp core check-update) != 'Success: WordPress is at the latest version.' || $(wp plugin update --all --dry-run) != 'No plugin updates available.' ]]; then

        curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' was checked for updates on '"$DATE"'. Updates Found.", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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

        if [[ $(wp core check-update) != 'Success: WordPress is at the latest version.' ]]; then
            wp core update
            wp core update-db
            git add .
            git commit -m "$COREUPDATE" --quiet
            curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' Updated Core", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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
        fi

        if [[ $(wp plugin update --all --dry-run) != 'No plugin updates available.' ]]; then
            wp plugin update --all
            git add .
            git commit -m "$PLUGINUPDATE" --quiet
            curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' Updated Plugins", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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
        fi

        if [[ $(wp core language update --dry-run) != 'Success: Translations are up to date.' ]]; then
            wp core language update
            git add .
            git commit -m "Updated Translations" --quiet
            curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' Updated Translations", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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
        fi

        git push origin updates --quiet

        # Accelo
        umask 000
        wget -O accelo.sh https://update.scripts.contradigital.co.uk/accelo.sh
        bash accelo.sh ${1} ${2}

    else

        curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' was checked for updates on '"$DATE"'. Nothing to update.", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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

    fi

fi

rm wordpress.sh