#!/bin/sh

# Arguments
BUILDID=$1

# Base Variables
PREV=$(readlink prev)
CURRENT=$(readlink current)
DATE=$(date +%F[%H:%M:%S])

# Unzip
unzip drop-$BUILDID.zip -d tmp-$BUILDID
mkdir drop-$BUILDID-$DATE
mv tmp-$BUILDID/**/* drop-$BUILDID-$DATE
rm -rf tmp-$BUILDID
rm drop-$BUILDID.zip

# Repoint Uploads
[ ! -d "uploads" ] && mkdir uploads && echo "No uploads folder...  Creating ./uploads"
ln -s ../../uploads drop-$BUILDID-$DATE/wp-content/uploads

# Copy root files into current
cd drop-$BUILDID-$DATE
ln -s ../.htaccess .htaccess && echo "Symlink .htacces"
cd -

# Backup the DB
cd drop-$BUILDID-$DATE
wp db export ../../tmp/backup-$CURRENT.sql && echo "Export DB to tmp/backup-$CURRENT.sql"
find ../../tmp/*.sql -mtime +14 -type f -delete
cd -

# Switching current and prev
[ $CURRENT ] && ln -sfn $CURRENT prev
ln -sfn drop-$BUILDID-$DATE current && echo "Switching current and prev"

# Flush the caches
cd drop-$BUILDID-$DATE
wp cache flush
# wp w3-total-cache flush all && echo "Flush caches"
cd -

# Delete the original PREV
[ $PREV ] && [ "$PREV" != "$CURRENT" ] && rm -rf $PREV && echo "Delete the original PREV"

# Remove self
rm azure-release-wordpress.sh
