#!/bin/sh

clear

echo "====================="
echo "Contra Drupal 8 Updates"
echo "====================="

cd ..
SITE="http://"
SITE+=$(cat conf/* | grep ServerAlias | cut -d" " -f 3)
cd public_html

git checkout updates
git fetch --all
git reset --hard origin/master
git pull --no-edit origin updates
git push origin updates

DATE=$(date +"%m/%d/%Y %r")
UPDATES=$(drush pm-updatestatus --format=csv --list-separator=' ')

STATUS=$(drush status 2>&1)

if [[ "$STATUS" =~ error* ]]; then

    curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "Error updating '"$SITE"' on '"$DATE"'.", "icon_emoji": ":bangbang:"}' https://hooks.slack.com/services/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

else

    if [ ! -z "$UPDATES" ]; then

        curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' was checked for updates on '"$DATE"'. Updates Found.", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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
        composer update drupal/core --with-dependencies
        drush updatedb
        drush cr
        drush up -y
        drush cr
        git add .
        git commit -m "$UPDATES" --quiet
        git push origin updates --quiet
        curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' - updates commits to branch", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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

        # Accelo
        umask 000
        wget -O accelo.sh https://update.scripts.contradigital.co.uk/accelo.sh
        bash accelo.sh ${1} ${2}

    else

        curl -X POST --data-urlencode 'payload={"channel": "#scheduled-updates", "username": "ContraRobot", "text": "'"$SITE"' was checked for updates on '"$DATE"'. Nothing to update.", "icon_emoji": ":robot_face:"}' https://hooks.slack.com/services/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

    fi

fi

rm drupal8.sh