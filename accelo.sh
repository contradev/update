#!/bin/sh

clear

echo "======"
echo "Acello"
echo "======"

# Accelo manager user id (Sally Born 6)
MANAGERID=6;
# Accelo user id (Sally Born 6)
ASSIGNEEID=6;
# Accelo user id (for automated task 'Roberto 2')
OWNERID=2;
# Billable time in seconds
BILLABLE=600;
# DateTime
DATETIME=$(date +%s)
# Site URL
cd ..
SITE="http://"
SITE+=$(cat conf/* | grep ServerAlias | cut -d" " -f 3)
cd public_html

# Get access_token
TOKEN=$(curl -X POST \
  https://contraagency.api.accelo.com/oauth2/v0/token.json \
  -H "authorization: Basic ${2}" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -d grant_type=client_credentials | python -c "import sys, json; print json.load(sys.stdin)['access_token']")

# Get retainer
RETAINER=$(curl -X GET \
  "https://contraagency.api.affinitylive.com/api/v0/contracts/${1}/periods?_filters=standing(opened)" \
  -H "authorization: Bearer ${TOKEN}" \
  -H 'cache-control: no-cache' \
  -H 'content-type: application/x-www-form-urlencoded' \
  -H 'postman-token: ab23b1ed-2a0e-abc7-fd9a-23da567e0e0d' | python -c "import sys, json; print json.load(sys.stdin)['response']['periods'][0]['id']")

# Check if Task has already been created and either log activity if already
# exists, or create a new task

# Log task ids to this file (to avoid creating again)
ACCELO_CREATED_TASK_ID_LOG=created_accelo_tasks.txt; # Log of each task that has been created (one per line)

# Use this flag to determine whether to create a new task or not
TASK_ALREADY_CREATED=false;

# Create log file if not there..
touch $ACCELO_CREATED_TASK_ID_LOG || exit

RAW_TITLE="Manually review ${SITE} following update, deploy to production server and check again"
TITLE=${RAW_TITLE// /%20}

# 1. First check the logged ids of tasks that have been created
#    To see if it already exists and either log an activity if it's still open
#    or remove the id from the log file if it's closed

# For each Task ID that has already been created
while read id; do

    # First get it to check if it's a valid task, and to get the title
    # to check if it's the same as the one about to be created and if the status still is open
    TASK=$(curl -X GET \
      "https://contraagency.api.affinitylive.com/api/v0/tasks/${id}?_fields=title,status" \
      -H "authorization: Bearer ${TOKEN}" \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/x-www-form-urlencoded');

    REQ_STATUS=$(echo $TASK | python -c "import sys, json; print json.load(sys.stdin).get('meta').get('status')");

    # If the Request was successful
    # NOTE double-square bracket required for == operator
    if [[ $REQ_STATUS == 'ok' ]];
    then
        TASK_STATUS=$(echo $TASK | python -c "import sys, json; print json.load(sys.stdin).get('response').get('status')");
        TASK_TITLE=$(echo $TASK | python -c "import sys, json; print json.load(sys.stdin).get('response').get('title')");

        # Is the task still open?
        TASK_IS_OPEN=false;
        #          Pending               Accepted             Accepted (start)
        if [[ $TASK_STATUS == '2' || $TASK_STATUS == '3' || $TASK_STATUS == '4' ]];
        then
            TASK_IS_OPEN=true;
        fi

        # NOTE double-square bracket required for == operator
        if [[ $TASK_IS_OPEN == true ]];
        then
            #echo "RAW_TITLE";
            #echo $RAW_TITLE;
            #echo "TASK_TITLE";
            #echo $TASK_TITLE;

            # If the title is the same as the title about to be created
            # create an activity for the task (and don't create another task)
            if [[ $TASK_TITLE == $RAW_TITLE ]];
            then
                echo "Task already created for this update, creating an activity instead."
                # IMPORTANT: This sets flag that task has already been created, so it isn't created again
                TASK_ALREADY_CREATED=true;
                ACTIVITY_SUBJECT="Further updates required.";
                ACTIVITY_SUBJECT=${ACTIVITY_SUBJECT// /%20};
                ACTIVITY_BODY="Addtional updates have been commited to the update branch"
                ACTIVITY_BODY=${ACTIVITY_BODY// /%20}
                curl -X POST \
                    "https://contraagency.api.affinitylive.com/api/v0/activities?subject=${ACTIVITY_SUBJECT}&body=${ACTIVITY_BODY}&against_id=${id}&against_type=task&owner_id=${OWNERID}&visibility=all&is_billable=no" \
                    -H "authorization: Bearer ${TOKEN}" \
                    -H 'cache-control: no-cache' \
                    -H 'content-type: application/x-www-form-urlencoded';
            fi
        else
            # Remove the id from the log if the Task is closed or the request was invalid
            # NOTE double-quotes are important here.. (needed for variable)
            sed -i -e "/^${id}\$/d" $ACCELO_CREATED_TASK_ID_LOG;
        fi
    fi

done < $ACCELO_CREATED_TASK_ID_LOG;

# 2. Then if the TASK_ALREADY_CREATED flag is still false
#    create the new task

if [[ $TASK_ALREADY_CREATED == false ]];
then
    # Create task
    DESCRIPTION=${SITE}
    DESCRIPTION+=" has been updated to the last version. Please check the updates which have been applied and merge these changes into the staging and live once tested. All time is billable to the retainer. Remember to update the processes file https://drive.google.com/drive/u/0/folders/0Bx7ysPuhyEXdMVBKY1g5U2RROU0 - *** Please complete this task was finished ***"
    DESCRIPTION=${DESCRIPTION// /%20}

    NEW_TASK=$(curl -X POST \
      "https://contraagency.api.affinitylive.com/api/v0/tasks?title=${TITLE}&description=${DESCRIPTION}&against_id=${RETAINER}&against_type=contract_period&date_started=${DATETIME}&manager_id=${MANAGERID}&assignee_id=${ASSIGNEEID}" \
      -H "authorization: Bearer ${TOKEN}" \
      -H 'cache-control: no-cache' \
      -H 'content-type: application/x-www-form-urlencoded');

    NEW_TASK_STATUS=$(echo $NEW_TASK | python -c "import sys, json; print json.load(sys.stdin).get('meta').get('status')");
    # NOTE double-square bracket required for == operator

    if [[ $NEW_TASK_STATUS == 'ok' ]];
    then
        # Log the task ID to the log file
        NEW_TASK_ID=$(echo $NEW_TASK | python -c "import sys, json; print json.load(sys.stdin).get('response').get('id')");
        echo $NEW_TASK_ID >> $ACCELO_CREATED_TASK_ID_LOG
    fi

    echo "new task id"
    echo $NEW_TASK_ID

    # Log billable time for new set off updates
    ACTIVITY_SUBJECT="Commited New Updates";
    ACTIVITY_SUBJECT=${ACTIVITY_SUBJECT// /%20};
    ACTIVITY_BODY="A new set of updates have been added to the updates branch"
    ACTIVITY_BODY=${ACTIVITY_BODY// /%20}
    curl -X POST \
        "https://contraagency.api.affinitylive.com/api/v0/activities?subject=${ACTIVITY_SUBJECT}&body=${ACTIVITY_BODY}&against_id=${NEW_TASK_ID}&against_type=task&owner_id=${OWNERID}&visibility=all&billable=${BILLABLE}&is_billable=yes" \
        -H "authorization: Bearer ${TOKEN}" \
        -H 'cache-control: no-cache' \
        -H 'content-type: application/x-www-form-urlencoded';

    echo "should create new activity under task"

fi
# ===============================================================================

rm accelo.sh