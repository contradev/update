<?php

function status($url) {
    $timeout = 10;
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
    $http_respond = curl_exec($ch);
    $http_respond = trim(strip_tags($http_respond));
    $http_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    if (($http_code == "200") || ($http_code == "302")) {
        echo '<span class="status up"></span>';
    } elseif (($http_code == "401")) {
        echo '<span class="status locked"></span>';
    } else {
        echo '<span class="status down"></span>';
    }
    curl_close($ch);
}

$sites = [
    'UK Safer Internet Centre' => 
    [
        'https://www.saferinternet.org.uk',
        'http://staging.uksic.contradigital.co.uk',
        'http://updates.uksic.contradigital.co.uk'
    ],
    'Corpus Christi College' =>
    [
        'http://www.corpus.cam.ac.uk',
        'http://staging.corpus.contradigital.co.uk',
        'http://updates.corpus.contradigital.co.uk'
    ],
    'Murray Edwards' =>
    [
        'https://www.murrayedwards.cam.ac.uk',
        'http://stage.murrayedwards.contradigital.co.uk',
        'http://updates.murrayedwards.contradigital.co.uk'
    ],
    'Youth Wellbeing' =>
    [
        'https://www.youthwellbeing.co.uk',
        'http://staging.ywb.contradigital.co.uk',
        'http://updates.youthwellbeing.contradigital.co.uk'
    ],
    'Good Salon Guide' => 
    [
        'https://www.goodsalonguide.com',
        'http://staging.gsg.contradigital.co.uk',
        'http://updates.gsg.contradigital.co.uk'
    ],
    'Gonville and Cauis College' => 
    [
        'http://staging.gonville.contradigital.co.uk',
        'http://updates.gonville.contradigital.co.uk'
    ],
    'Downing College' => 
    [
        'http://staging.downing.contradigital.co.uk',
        'http://updates.downing.contradigital.co.uk'
    ],
    'Liquid Capital' =>
    [
        'http://staging.liquidcapital.contradigital.co.uk'
    ],
    'My HE Plus' =>
    [
        'http://staging.myheplus.contradigital.co.uk'
    ]
];

?>


<html lang="en">
    <head>
        <title>Site Updates</title>
        <style>
            body {
                font-family: "HelveticaNeue-Light", "Helvetica Neue Light", "Helvetica Neue", Helvetica, Arial, "Lucida Grande", sans-serif; 
                font-weight: 300;
                color: #333333;
            }
            h1, h2 {
                font-weight: 300;
            }
            h2 {
                margin-bottom: 10px;
                font-size: 18px;
            }
            ul {
                list-style: none;
                margin-bottom: 20px;
            }
            a {
                color: #333333;
                text-decoration-color: #0074D9;
            }
            a:hover {
                color: #0074D9;
                text-decoration: none;
            }
            .status {
                position: relative;
                display: inline-block;
                border-radius: 100%;
                width: 8px;
                height: 8px;
            }
            .status.up {
                background: #2ECC40;
            }
            .status.down {
                background: #FF4136;
            }
            .status.locked {
                background: #FFDC00;
            }
            .status.locked::before {
                content: '\01F510';
                position: absolute;
                top: -10px;
                left: -20px;
            }
        </style>
    </head>
    <body>
        <h1>Sites</h1>

        <?php foreach ($sites as $key => $value) { ?>
            <div>
                <h2><?php echo $key ?></h2>
                <ul>
                    <?php foreach ($value as $url) { ?>
                        <li><?php status($url) ?> <a href="<?= $url ?>"><?= $url ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        <?php } ?>

    </body>
</html>