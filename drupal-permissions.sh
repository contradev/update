#!/bin/sh

echo "Reset Permissions"

# variables passed in order..
# EMAIL API_KEY APP_ID SERVER_ID

EMAIL=$1
API_KEY=$2
APP_ID=$3
SERVER_ID=$4

TOKEN=$(curl -X POST --header "Content-Type: application/x-www-form-urlencoded" --header "Accept: application/json" -d "email=${EMAIL}&api_key=${API_KEY}" "https://api.cloudways.com/api/v1/oauth/access_token" | python -c "import sys, json; print json.load(sys.stdin)['access_token']")
echo $(curl -s -X POST --header "Content-Type: application/x-www-form-urlencoded" --header "Accept: application/json" --header "Authorization: Bearer ${TOKEN}" -d "server_id=${SERVER_ID}&app_id=${APP_ID}" "https://api.cloudways.com/api/v1/app/manage/reset_permissions")

sleep 30s

printf "Changing permissions of all directories to "rwxr-x---"...\n"
find . -type d -exec chmod u=rwx,g=rx,o= '{}' \;

printf "Changing permissions of all files to "rw-r-----"...\n"
find . -type f -exec chmod u=rw,g=r,o= '{}' \;

printf "Changing permissions of "files" directories in "/sites" to "rwxrwx---"...\n"
cd sites
find . -type d -name files -exec chmod ug=rwx,o= '{}' \;

printf "Changing permissions of all files inside all "files" directories in "/sites" to "rw-rw----"...\n"
printf "Changing permissions of all directories inside all "files" directories in "/sites" to "rwxrwx---"...\n"
for x in ./*/files; do
    find ${x} -type d -exec chmod ug=rwx,o= '{}' \;
    find ${x} -type f -exec chmod ug=rw,o= '{}' \;
done
echo "Done setting proper permissions on files and directories"

cd ../

find cache -type d -exec chmod u=rwx,g=rx,o=rx '{}' \;

rm reset.sh
